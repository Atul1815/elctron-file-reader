import { NgZone } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ElectronService } from '../../providers/electron.service';
import * as XLSX from 'xlsx';
import { of, Observable, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  content = new BehaviorSubject<any>([]);
  // getData = this.content.asObservable();
getData: any;
  constructor(private electron: ElectronService, private zone: NgZone) {}

  ngOnInit() {
    // this.getData = ['a', 'b'];
    // this.content.next(["a", "b"]);
    console.log(this.content);
  }
  onSelectFile() {
    const that = this;
    const fs = this.electron.fs;
    this.electron.remote.dialog.showOpenDialog(
      {
        defaultPath: './',
        buttonLabel: 'Please select File'
      },
      file => {
        fs.readFile(file[0], function(err, data) {
          if (err) {
            return console.error(err);
          }
          const x: XLSX.WorkBook = XLSX.readFile(file[0], { type: 'string' });
          const wsname: string = x.SheetNames[0];
          const ws: XLSX.WorkSheet = x.Sheets[wsname];
          const fileData = XLSX.utils.sheet_to_json(ws, { header: 1 });

          that.zone.run(() => {
            console.log(fileData);
            that.getData = fileData;
            console.log(that.getData);
          });
        });
      }
    );
  }
}
